import { dispatch } from '../AppDispatcher'
import { UPDATE_MESSAGE } from '../constants'
import HelloStore from '../stores/HelloStore'

export function updateStoreMessage(newStoreMessage) {
  dispatch(UPDATE_MESSAGE, { newStoreMessage })
}