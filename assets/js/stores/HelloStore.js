import { register } from '../AppDispatcher'
import { createStore } from '../utils/StoreUtils'
import { UPDATE_MESSAGE } from '../constants'

let storeMessage = "Am I working?"

const HelloStore = createStore({
    get(){
        return storeMessage    
    }
})

register(action => {
    switch (action.type) {
        case UPDATE_MESSAGE:
            storeMessage = action.newStoreMessage
            HelloStore.emitChange()
            break
    }
})


export default HelloStore