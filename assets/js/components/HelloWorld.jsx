import React from 'react'
import HelloStore from '../stores/HelloStore'
import * as HelloWorldActions from '../actions/HelloWorldActions'

function getHelloState() {
    return {
        storeMessage: HelloStore.get()
    }
}

export default class HelloWorld extends React.Component {
    constructor(props) {
        super(props)
        this.state = getHelloState()
        this._onChange = this._onChange.bind(this)
    }
    
    componentDidMount() {
        HelloStore.addChangeListener(this._onChange)
    }

    componentWillUnmount() {
        HelloStore.removeChangeListener(this._onChange)
    }

    _onChange() {
        this.setState(getHelloState())
    }

    _onClick() {
        const newMessage = "Yes I am :)"
        HelloWorldActions.updateStoreMessage(newMessage)
    }

    render(){
        return (
            <div className="hello-world">
                <p>Hello {this.props.message}!</p>
                <p>{this.state.storeMessage}</p>
                <p>
                    <button type="button" onClick={this._onClick}>Try me</button>
                </p>
            </div>
        )
    }
}